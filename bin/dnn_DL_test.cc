/*
 * DNN DL classification test.
 */

#include <iostream>
#include <limits>
#include <sys/time.h>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

#ifndef EPSILON
#define EPSILON 1.0e-6
#endif

const TLorentzVector makeVector(double pt, double eta, double phi, double mass)
{
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

int test_DNN_DL_v0();

int main()
{
    DNNClassifierBase::pyInitialize();
    test_DNN_DL_v0();
    DNNClassifierBase::pyFinalize();
    return 0;
}

int test_DNN_DL_v0()
{
    // set precision of numbers in cout
    std::cout.precision(8);

    // setup the dnn classifier
    DNNClassifier_DL dnn("v0");

    DNNOutput output;

    // dummy model
    std::vector<TLorentzVector> jets = {
        makeVector(82.193317445, -1.381465673, 0.875596046, 12.354169733),
        makeVector(61.222326911, -0.624968886, 1.280390978, 7.378321245),
        makeVector(49.268722080, -1.523553133, 2.887234688, 10.714914255),
        makeVector(45.137310817, -0.522410452, 2.953480721, 4.287899215)
    };
    std::vector<double> jetCSVs = { 0.951796949, 0.167348146, 0.164827660, 0.804015398 };

    std::vector<TLorentzVector> leptons = {
        makeVector(126.932662964, 0.030971697, -0.758532584, 0.105700001),
        makeVector(126.932662964, 0.030971697, -0.758532584, 0.105700001)
    };
    TLorentzVector met = makeVector(49.720878601, 0., -2.168250561, 0.);

    // evaluate
    output = dnn.evaluate(jets, jetCSVs, leptons, met);

    // some output
    std::vector<DNNOutput> outputs = { output };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        // std::cout << "evaluation for model " << (i + 3) << "j:" << std::endl;
        std::cout << "evaluation for dummy model" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    // validate outputs
    std::vector<double> targetOutputs = { 0., 0., 0., 0., 0., 0., 0. };

    for (size_t i = 0; i < targetOutputs.size(); ++i)
    {
        assert(fabs(targetOutputs[i] - output.values[i]) < EPSILON && "The DNN output for dummy events is incorrect");
    }

    // performance test
    std::cout << "performance test:" << std::endl;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int t0 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    size_t n = 50000;
    for (size_t i = 0; i < n; ++i)
    {
        dnn.evaluate(jets, jetCSVs, leptons, met);
    }
    gettimeofday(&tp, NULL);
    long int t1 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    int diff = t1 - t0;
    std::cout << n << " evalutions took " << (diff / 1000.) << " seconds" << std::endl;
    std::cout << "=> " << (((float)diff) / n) << " ms per evaluation" << std::endl;

    return 0;
}

#undef EPSILON

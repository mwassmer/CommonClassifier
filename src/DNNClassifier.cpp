/*
 * DNN Classifier.
 * Please note that this classifier actually outputs 7 discriminator values
 * simultaneously. For the moment, the DNN's only work in the SL channel.
 * They can be interpreted as a classification probability as they sum up to 1.
 * Classes (order is important!): ttH, ttbb, ttb, tt2b, ttcc, ttlf, other
 */

#include <iostream>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

DNNClassifierBase::DNNClassifierBase(std::string version)
    : csvCut(0.8484)
    , version_(version)
    , inputName_("inp")
    , outputName_("outp")
    , dropoutName_("keep_prob")
    , pyContext_(0)
    , pyEval_(0)
{
}

DNNClassifierBase::~DNNClassifierBase()
{
    // cleanup python objects
    if (pyEval_) Py_DECREF(pyEval_);
    if (pyContext_) Py_DECREF(pyContext_);
}

void DNNClassifierBase::pyInitialize()
{
    PyEval_InitThreads();
    Py_Initialize();
}

void DNNClassifierBase::pyFinalize()
{
    Py_Finalize();
}

DNNClassifier_SL::DNNClassifier_SL(std::string version)
    : DNNClassifierBase(version)
    , nFeatures4_(0)
    , nFeatures5_(0)
    , nFeatures6_(0)
    , pyEvalArgs4_(0)
    , pyEvalArgs5_(0)
    , pyEvalArgs6_(0)
{
    // set feature numbers based in the version
    if (version_ == "v2")
    {
        nFeatures4_ = 39;
        nFeatures5_ = 44;
        nFeatures6_ = 49;
    }
    else if (version_ == "v3")
    {
        nFeatures4_ = 30;
        nFeatures5_ = 34;
        nFeatures6_ = 38;
    }
    else
    {
        throw std::runtime_error("unknown version: " + version_);
    }

    // determine some local paths
    std::string cmsswBase = std::string(getenv("CMSSW_BASE"));
    std::string tfdeployBase = cmsswBase + "/python/TTH/CommonClassifier";
    std::string modelsBase = cmsswBase + "/src/TTH/CommonClassifier/data/dnnmodels_SL_" + version_;
    std::string modelFile4 = modelsBase + "/model_4j.pkl";
    std::string modelFile5 = modelsBase + "/model_5j.pkl";
    std::string modelFile6 = modelsBase + "/model_6j.pkl";

    // initialize the python main object, load the script
    PyObject* pyMainModule = PyImport_AddModule("__main__");

    PyObject* pyMainDict = PyModule_GetDict(pyMainModule);
    pyContext_ = PyDict_Copy(pyMainDict);

    PyRun_String(evalScript.c_str(), Py_file_input, pyContext_, pyContext_);

    // load the tfdeploy models
    PyObject* pySetup = PyDict_GetItemString(pyContext_, "setup");
    PyObject* pyModelFiles = PyTuple_New(3);
    PyTuple_SetItem(pyModelFiles, 0, PyString_FromString(modelFile4.c_str()));
    PyTuple_SetItem(pyModelFiles, 1, PyString_FromString(modelFile5.c_str()));
    PyTuple_SetItem(pyModelFiles, 2, PyString_FromString(modelFile6.c_str()));
    PyObject* pyArgs = PyTuple_New(5);
    PyTuple_SetItem(pyArgs, 0, PyString_FromString(tfdeployBase.c_str()));
    PyTuple_SetItem(pyArgs, 1, pyModelFiles);
    PyTuple_SetItem(pyArgs, 2, PyString_FromString(inputName_.c_str()));
    PyTuple_SetItem(pyArgs, 3, PyString_FromString(outputName_.c_str()));
    PyTuple_SetItem(pyArgs, 4, PyString_FromString(dropoutName_.c_str()));

    PyObject* pyResult = PyObject_CallObject(pySetup, pyArgs);
    if (pyResult == NULL)
    {
        if (PyErr_Occurred() != NULL)
        {
            PyErr_PrintEx(0);
        }
        throw runtime_error("an error occured while loading the tfdeploy models");
    }

    // store the evaluation function and prepare args
    // the "+ 1" is due to the model number being the first argument in the eval function
    pyEval_ = PyDict_GetItemString(pyContext_, "eval");
    pyEvalArgs4_ = PyTuple_New(nFeatures4_ + 1);
    pyEvalArgs5_ = PyTuple_New(nFeatures5_ + 1);
    pyEvalArgs6_ = PyTuple_New(nFeatures6_ + 1);
}

DNNClassifier_SL::~DNNClassifier_SL()
{
    // cleanup python objects
    if (pyEvalArgs4_) Py_DECREF(pyEvalArgs4_);
    if (pyEvalArgs5_) Py_DECREF(pyEvalArgs5_);
    if (pyEvalArgs6_) Py_DECREF(pyEvalArgs6_);
}

void DNNClassifier_SL::evaluate(const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met,
    DNNOutput& dnnOutput)
{
    size_t nJets = jets.size();
    size_t modelNum;
    PyObject* pyEvalArgs = NULL;
    if (nJets < 4)
    {
        throw runtime_error("no DNN classifier existing for < 4 jets");
    }
    else if (nJets == 4)
    {
        pyEvalArgs = pyEvalArgs4_;
        modelNum = 0;
    }
    else if (nJets == 5)
    {
        pyEvalArgs = pyEvalArgs5_;
        modelNum = 1;
    }
    else
    {
        pyEvalArgs = pyEvalArgs6_;
        modelNum = 2;
    }

    // modelNum is at pos 0
    PyTuple_SetItem(pyEvalArgs, 0, PyInt_FromSize_t(modelNum));

    // fill features into the py tuple
    fillFeatures_(pyEvalArgs, jets, jetCSVs, lepton, met);

    // evaluate
    PyObject* pyList = PyObject_CallObject(pyEval_, pyEvalArgs);

    // fill the dnnOutput
    dnnOutput.values.resize(7);
    for (size_t i = 0; i < (version_ == "v2" ? 7 : 6); i++)
    {
        dnnOutput.values[i] = PyFloat_AsDouble(PyList_GetItem(pyList, i));
    }
    if (version_ != "v2") // no 'other' class
    {
        dnnOutput.values[6] = 0.0;
    }

    Py_DECREF(pyList);
}

DNNOutput DNNClassifier_SL::evaluate(const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met)
{
    DNNOutput dnnOutput;
    evaluate(jets, jetCSVs, lepton, met, dnnOutput);
    return dnnOutput;
}

void DNNClassifier_SL::fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met)
{
    size_t idx = 1;

    // low-level jet features: pt, eta, phi, mass, csv
    for (size_t i = 0; i < min(jets.size(), (size_t)6); i++)
    {
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Eta()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Phi()));
        if (version_ == "v2")
        {
            PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Mag()));
        }
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[i]));
    }

    // low-level lepton features: pt, eta, phi, mass
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Pt()));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Eta()));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Phi()));
    if (version_ == "v2")
    {
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Mag()));

        // low-level met features: pt, phi
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(met.Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(met.Phi()));
    }

    // split jets into b jets and light jets
    // store pointers to avoid performance drawbacks due to vector copying
    std::vector<const TLorentzVector*> allJets;
    std::vector<const TLorentzVector*> bJets;
    std::vector<double> bCSVs;
    std::vector<const TLorentzVector*> lJets;
    std::vector<double> lCSVs;
    for (size_t i = 0; i < jets.size(); i++)
    {
        allJets.push_back(&jets[i]);
        if (jetCSVs[i] >= csvCut)
        {
            bJets.push_back(&jets[i]);
            bCSVs.push_back(jetCSVs[i]);
        }
        else
        {
            lJets.push_back(&jets[i]);
            lCSVs.push_back(jetCSVs[i]);
        }
    }

    // Ht
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getHt(allJets)));

    // min and max dR between jets
    double minDRJets, maxDRJets;
    dnnVars_.getMinMaxDR(allJets, minDRJets, maxDRJets);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJets));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRJets));

    // min and max dR between light jets
    if (version_ == "v2")
    {
        double minDRLJets, maxDRLJets;
        dnnVars_.getMinMaxDR(lJets, minDRLJets, maxDRLJets);
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRLJets));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRLJets));
    }

    // min and max dR between b jets
    double minDRBJets, maxDRBJets;
    dnnVars_.getMinMaxDR(bJets, minDRBJets, maxDRBJets);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRBJets));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRBJets));

    // min and max dR between b jets and the lepton
    double minDRBJetsLep, maxDRBJetsLep;
    dnnVars_.getMinMaxDR(bJets, &lepton, minDRBJetsLep, maxDRBJetsLep);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRBJetsLep));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRBJetsLep));

    // jet centrality
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(dnnVars_.getCentrality(allJets)));

    // jet aplanarity, sphericity and transverse sphericity
    double ev1, ev2, ev3;
    dnnVars_.getSphericalEigenValues(allJets, ev1, ev2, ev3);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3)));
    double tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

    // Fox-Wolfram moments
    // disabled for the moment, see https://gitlab.cern.ch/ttH/CommonClassifier/issues/1
    // double fw1, fw2, fw3, fw4, fw5;
    // bdtVars_.getFox(jets, fw1, fw2, fw3, fw4, fw5);
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw1));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw2));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw3));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw4));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw5));
}

DNNClassifier_DL::DNNClassifier_DL(std::string version)
    : DNNClassifierBase(version)
    , nFeatures3_(0)
    , nFeatures4_(0)
    , pyEvalArgs3_(0)
    , pyEvalArgs4_(0)
{
    // set feature numbers based in the version
    // nothing to do yet
    if (version_ != "v0")
    {
        throw std::runtime_error("unknown version: " + version_);
    }

    // determine some local paths
    std::string cmsswBase = std::string(getenv("CMSSW_BASE"));
    std::string tfdeployBase = cmsswBase + "/python/TTH/CommonClassifier";
    std::string modelsBase = cmsswBase + "/src/TTH/CommonClassifier/data/dnnmodels_DL_" + version_;
    std::string modelFile3 = modelsBase + "/model_3j.pkl";
    std::string modelFile4 = modelsBase + "/model_4j.pkl";

    // // initialize the python main object, load the script
    // PyObject* pyMainModule = PyImport_AddModule("__main__");

    // PyObject* pyMainDict = PyModule_GetDict(pyMainModule);
    // pyContext_ = PyDict_Copy(pyMainDict);

    // PyRun_String(evalScript.c_str(), Py_file_input, pyContext_, pyContext_);

    // // load the tfdeploy models
    // PyObject* pySetup = PyDict_GetItemString(pyContext_, "setup");
    // PyObject* pyModelFiles = PyTuple_New(2);
    // PyTuple_SetItem(pyModelFiles, 1, PyString_FromString(modelFile3.c_str()));
    // PyTuple_SetItem(pyModelFiles, 0, PyString_FromString(modelFile4.c_str()));
    // PyObject* pyArgs = PyTuple_New(5);
    // PyTuple_SetItem(pyArgs, 0, PyString_FromString(tfdeployBase.c_str()));
    // PyTuple_SetItem(pyArgs, 1, pyModelFiles);
    // PyTuple_SetItem(pyArgs, 2, PyString_FromString(inputName_.c_str()));
    // PyTuple_SetItem(pyArgs, 3, PyString_FromString(outputName_.c_str()));
    // PyTuple_SetItem(pyArgs, 4, PyString_FromString(dropoutName_.c_str()));

    // PyObject* pyResult = PyObject_CallObject(pySetup, pyArgs);
    // if (pyResult == NULL)
    // {
    //     if (PyErr_Occurred() != NULL)
    //     {
    //         PyErr_PrintEx(0);
    //     }
    //     throw runtime_error("an error occured while loading the tfdeploy models");
    // }

    // // store the evaluation function and prepare args
    // // the "+ 1" is due to the model number being the first argument in the eval function
    // pyEval_ = PyDict_GetItemString(pyContext_, "eval");
    // pyEvalArgs3_ = PyTuple_New(nFeatures3_ + 1);
    // pyEvalArgs4_ = PyTuple_New(nFeatures4_ + 1);
}

DNNClassifier_DL::~DNNClassifier_DL()
{
    // cleanup python objects
    if (pyEvalArgs3_) Py_DECREF(pyEvalArgs3_);
    if (pyEvalArgs4_) Py_DECREF(pyEvalArgs4_);
}

void DNNClassifier_DL::evaluate(const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const std::vector<TLorentzVector>& leptons,
    const TLorentzVector& met, DNNOutput& dnnOutput)
{
    // size_t nJets = jets.size();
    // size_t modelNum;
    // PyObject* pyEvalArgs = NULL;
    // if (nJets < 3)
    // {
    //     throw runtime_error("no DNN classifier existing for < 3 jets");
    // }
    // else if (nJets == 3)
    // {
    //     pyEvalArgs = pyEvalArgs3_;
    //     modelNum = 0;
    // }
    // else
    // {
    //     pyEvalArgs = pyEvalArgs4_;
    //     modelNum = 1;
    // }

    // // modelNum is at pos 0
    // PyTuple_SetItem(pyEvalArgs, 0, PyInt_FromSize_t(modelNum));

    // // fill features into the py tuple
    // fillFeatures_(pyEvalArgs, jets, jetCSVs, leptons, met);

    // // evaluate
    // PyObject* pyList = PyObject_CallObject(pyEval_, pyEvalArgs);

    // // fill the dnnOutput
    // dnnOutput.values.resize(7);
    // for (size_t i = 0; i < 7; i++)
    // {
    //     dnnOutput.values[i] = PyFloat_AsDouble(PyList_GetItem(pyList, i));
    // }

    // Py_DECREF(pyList);
}

DNNOutput DNNClassifier_DL::evaluate(const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const std::vector<TLorentzVector>& leptons,
    const TLorentzVector& met)
{
    DNNOutput dnnOutput;
    evaluate(jets, jetCSVs, leptons, met, dnnOutput);
    return dnnOutput;
}

void DNNClassifier_DL::fillFeatures_(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const std::vector<TLorentzVector>& leptons,
    const TLorentzVector& met)
{
}

double DNNVariables::getHt(const std::vector<const TLorentzVector*>& lvecs) const
{
    double ht = 0;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        ht += lvecs[i]->Pt();
    }
    return ht;
}

void DNNVariables::getMinMaxDR(
    const std::vector<const TLorentzVector*>& lvecs, double& minDR, double& maxDR) const
{
    maxDR = lvecs.size() == 0 ? -1 : 0.;
    minDR = lvecs.size() == 0 ? -1 : 100.;
    if (lvecs.size() >= 2)
    {
        for (size_t i = 0; i < lvecs.size() - 1; i++)
        {
            for (size_t j = i + 1; j < lvecs.size(); j++)
            {
                double dR = lvecs[i]->DeltaR(*lvecs[j]);
                if (dR > maxDR)
                {
                    maxDR = dR;
                }
                if (dR < minDR)
                {
                    minDR = dR;
                }
            }
        }
    }
}

void DNNVariables::getMinMaxDR(const std::vector<const TLorentzVector*>& lvecs,
    const TLorentzVector* lvec, double& minDR, double& maxDR) const
{
    maxDR = lvecs.size() == 0 ? -1 : 0.;
    minDR = lvecs.size() == 0 ? -1 : 100.;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        double dR = lvecs[i]->DeltaR(*lvec);
        if (dR > maxDR)
        {
            maxDR = dR;
        }
        if (dR < minDR)
        {
            minDR = dR;
        }
    }
}

double DNNVariables::getCentrality(const std::vector<const TLorentzVector*>& lvecs) const
{
    double sumPt = 0.;
    double sumP = 0.;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        sumPt += lvecs[i]->Pt();
        sumP += lvecs[i]->P();
    }
    return sumPt / sumP;
}

void DNNVariables::getSphericalEigenValues(
    const std::vector<const TLorentzVector*>& lvecs, double& ev1, double& ev2, double& ev3) const
{
    TMatrixDSym momentumMatrix(3);
    double p2Sum = 0.;

    for (size_t i = 0; i < lvecs.size(); i++)
    {
        double px = lvecs[i]->Px();
        double py = lvecs[i]->Py();
        double pz = lvecs[i]->Pz();

        // fill the matrix
        momentumMatrix(0, 0) += px * px;
        momentumMatrix(0, 1) += px * py;
        momentumMatrix(0, 2) += px * pz;
        momentumMatrix(1, 0) += py * px;
        momentumMatrix(1, 1) += py * py;
        momentumMatrix(1, 2) += py * pz;
        momentumMatrix(2, 0) += pz * px;
        momentumMatrix(2, 1) += pz * py;
        momentumMatrix(2, 2) += pz * pz;

        // add 3 momentum squared to sum
        p2Sum += px * px + py * py + pz * pz;
    }

    // normalize each element by p2Sum
    if (p2Sum != 0.)
    {
        for (size_t i = 0; i < 3; i++)
        {
            for (size_t j = 0; j < 3; j++)
            {
                momentumMatrix(i, j) = momentumMatrix(i, j) / p2Sum;
            }
        }
    }

    // calculatate eigen values via eigen vectors
    TMatrixDSymEigen eig(momentumMatrix);
    TVectorD ev = eig.GetEigenValues();

    // some checks due to limited precision of TVectorD
    ev1 = fabs(ev[0]) < 0.00000000000001 ? 0 : ev[0];
    ev2 = fabs(ev[1]) < 0.00000000000001 ? 0 : ev[1];
    ev3 = fabs(ev[2]) < 0.00000000000001 ? 0 : ev[2];
}
